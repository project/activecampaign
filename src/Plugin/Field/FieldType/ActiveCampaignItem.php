<?php

namespace Drupal\activecampaign\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'activeCampaign' field type.
 *
 * @FieldType(
 *   id = "active_campaign_field",
 *   label = @Translation("Active campaign field"),
 *   description = @Translation("Stores the ActiveCampaign form id"),
 *   default_widget = "activecampaign_form_select_list",
 *   default_formatter = "activecampaign_form"
 * )
 */
class ActiveCampaignItem extends StringItemBase {

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Form id'));

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'description' => t('The active campaign form id.'),
        ],
      ],
    ];
  }

}
