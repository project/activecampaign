<?php

namespace Drupal\activecampaign\Plugin\Field\FieldWidget;

use Drupal\activecampaign\ActiveCampaignApi;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'activecampaign_form_autocomplete' widget.
 *
 * @FieldWidget(
 *   id = "activecampaign_form_autocomplete",
 *   label = @Translation("Autocomplete Active campaign forms list"),
 *   field_types = {
 *     "active_campaign_field"
 *   }
 * )
 */
class ActiveCampaignFormAutocompleteWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The active campaign api service.
   *
   * @var \Drupal\activecampaign\ActiveCampaignApi
   */
  protected $activeCampaignApi;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\activecampaign\ActiveCampaignApi $active_campaign_api
   *   The active campaign api service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ActiveCampaignApi $active_campaign_api
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );
    $this->activeCampaignApi = $active_campaign_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('activecampaign.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items,
                              $delta,
                              array $element,
                              array &$form,
                              FormStateInterface $form_state): array {

    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';
    $title = '';

    if ($value !== '') {
      $title = $this->activeCampaignApi->getFormTitle($value);
    }

    $element += [
      '#type' => 'textfield',
      '#default_value' => $value === '' ? '' : $title . ' (' . $value . ')',
      '#autocomplete_route_name' => 'activecampaign.autocomplete.forms',
    ];
    return ['value' => $element];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $old_value = $values[0]['value'];
    $values[0]['value'] = EntityAutocomplete::extractEntityIdFromAutocompleteInput($old_value);
    return parent::massageFormValues($values, $form, $form_state);
  }

}
