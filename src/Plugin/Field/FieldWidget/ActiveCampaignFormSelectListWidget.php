<?php

namespace Drupal\activecampaign\Plugin\Field\FieldWidget;

use Drupal\activecampaign\ActiveCampaignApi;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'activecampaign_form_select_list' widget.
 *
 * @FieldWidget(
 *   id = "activecampaign_form_select_list",
 *   label = @Translation("Active campaign forms select list"),
 *   field_types = {
 *     "active_campaign_field"
 *   }
 * )
 */
class ActiveCampaignFormSelectListWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The active campaign api service.
   *
   * @var \Drupal\activecampaign\ActiveCampaignApi
   */
  protected $activeCampaignApi;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\activecampaign\ActiveCampaignApi $active_campaign_api
   *   The active campaign api service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ActiveCampaignApi $active_campaign_api
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );
    $this->activeCampaignApi = $active_campaign_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('activecampaign.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items,
                              $delta,
                              array $element,
                              array &$form,
                              FormStateInterface $form_state): array {

    $ac_forms = $this->activeCampaignApi->getForms();
    $default_radio_value = NULL;
    $options = [];

    // Build the select list names.
    foreach ($ac_forms as $ac_form) {
      $form_name = $ac_form['name'];
      // If the form has a value, put this id as the default.
      if ($items[$delta]->value === $ac_form['id']) {
        $default_radio_value = $ac_form['id'];
      }
      $options[$ac_form['id']] = $form_name;
    }

    $element += [
      '#type' => 'select',
      '#title' => $this->t('All forms'),
      '#default_value' => $default_radio_value,
      '#options' => $options,
    ];

    return ['value' => $element];
  }

}
