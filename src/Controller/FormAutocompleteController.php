<?php

namespace Drupal\activecampaign\Controller;

use Drupal\activecampaign\ActiveCampaignApi;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a route controller for autocomplete form elements.
 */
class FormAutocompleteController extends ControllerBase {

  /**
   * The active campaign api service.
   *
   * @var \Drupal\activecampaign\ActiveCampaignApi
   */
  protected $activeCampaignApi;

  /**
   * Construct FormAutoCompleteController class.
   *
   * @param \Drupal\activecampaign\ActiveCampaignApi $active_campaign_api
   *   The active campaign api service.
   */
  public function __construct(ActiveCampaignApi $active_campaign_api) {
    $this->activeCampaignApi = $active_campaign_api;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      $container->get('activecampaign.api')
    );
  }

  /**
   * Handler for autocomplete request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   HTTP request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Return form list as Json.
   */
  public function handleAutocomplete(Request $request): JsonResponse {
    $results = [];
    $input = $request->query->get('q');

    // Get the typed string from the URL, if it exists.
    if (!$input) {
      return new JsonResponse($results);
    }

    $input = Xss::filter($input);

    $list = $this->activeCampaignApi->searchForms($input);

    $results = [];
    foreach ($list as $item) {
      array_push($results, [
        'label' => $item['name'],
        'value' => $item['name'] . ' (' . $item['id'] . ')',
      ]);
    }
    return new JsonResponse($results);
  }

}
