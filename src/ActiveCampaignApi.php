<?php

namespace Drupal\activecampaign;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;

/**
 * Api service.
 */
class ActiveCampaignApi {

  /**
   * The active campaign api SDK.
   *
   * @var \ActiveCampaign
   */
  protected $activeCampaignSDK;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The url to active campaign.
   *
   * @var array|mixed|null
   */
  protected $url;

  /**
   * Constructs an Api object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
    $config = $this->configFactory->get('activecampaign.settings');
    $this->url = $config->get('url');
    $this->activeCampaignSDK = new \ActiveCampaign($config->get('api_url'), $config->get('api_key'));
  }

  /**
   * Returns an array of form names with id's.
   *
   * @return array
   *   A multidimensional array of form names and id's.
   *   The array keys are [id] and [name].
   */
  public function getForms(): array {
    $forms = $this->activeCampaignSDK->api('form/getforms');
    $foundForms = [];
    foreach ($forms as $form) {
      if (is_object($form) && isset($form->id)) {
        $foundForms[] = [
          'id' => $form->id,
          'name' => $form->name,
        ];
      }
    }
    return $foundForms;
  }

  /**
   * Returns an array of form names with id's that contain the given string.
   *
   * @param string $query
   *   The characters that should be present in the form name.
   *
   * @return array
   *   A multidimensional array of form names and id's.
   *   The array keys are [id] and [name].
   */
  public function searchForms(string $query): array {
    $forms = $this->getForms();
    $foundForms = [];
    foreach ($forms as $form) {
      if (strpos(strtolower($form['name']), strtolower($query)) !== FALSE) {
        $foundForms[] = $form;
      }
    }
    return $foundForms;
  }

  /**
   * Get a contact via the active campaign API.
   *
   * @param int $contact_id
   *   The contact ID.
   */
  public function getContact(int $contact_id) {
    $options['api_output'] = 'json';
    $options['id'] = $contact_id;
    $query = UrlHelper::buildQuery($options);
    return $this->activeCampaignSDK->api('contact/view?' . $query);
  }

  /**
   * Get contacts via the active campaign API.
   *
   * @param array $filter_options
   *   The filter options for the api call.
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of records per page.
   */
  public function getContacts(array $filter_options = [], int $page = 0, int $limit = 20) {
    $options['ids'] = 'all';
    $options['api_output'] = 'json';
    $options['sort'] = '03D';
    $options['limit'] = $limit;
    $options['offset'] = $page * $limit;
    $query = UrlHelper::buildQuery($options);
    return $this->activeCampaignSDK->api('contact/paginator?' . $query);
  }

  /**
   * Get lists via the active campaign API.
   *
   * @param array $filter_options
   *   The filter options for the api call.
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of records per page.
   */
  public function getLists(array $filter_options = [], int $page = 0, int $limit = 20) {
    $options['ids'] = 'all';
    $options['api_output'] = 'json';
    $options['limit'] = $limit;
    $options['offset'] = $page * $limit;
    $query = UrlHelper::buildQuery($options);
    return $this->activeCampaignSDK->api('list/paginator?' . $query);
  }

  /**
   * Get Campaigns via the active campaign API.
   *
   * @param array $filter_options
   *   The filter options for the api call.
   * @param int $page
   *   The page to start from.
   * @param int $limit
   *   The number of records per page.
   */
  public function getCampaigns(array $filter_options = [], int $page = 0, int $limit = 20) {
    $options['ids'] = 'all';
    $options['api_output'] = 'json';
    $options['limit'] = $limit;
    $options['offset'] = $page * $limit;
    $query = UrlHelper::buildQuery($options);
    return $this->activeCampaignSDK->api('campaign/paginator?' . $query);
  }

  /**
   * Sends contact data to active campaign using the active campaign API.
   *
   * @param array $contact
   *   The data to send to the active campaign site.
   */
  public function syncContact(array $contact) {
    return $this->activeCampaignSDK->api('contact/sync', $contact);
  }

  /**
   * Return the form name of a form with the given id.
   *
   * @param string $value
   *   The id of the form of which the title should be retrieved.
   *
   * @return string
   *   The form title from the form with given id or een empty string.
   */
  public function getFormTitle(string $value): string {
    $forms = $this->getForms();
    foreach ($forms as $form) {
      if ($form['id'] == $value) {
        return $form['name'];
      }
    }
    // When no form with the given name is found, return an empty string.
    return '';
  }

  /**
   * Create a URL to a contact.
   *
   * @param int $contact_id
   *   The id of the contact to return.
   *
   * @return \Drupal\Core\Url
   *   Link to the details page on active campaign of the contact with given id.
   */
  public function createUrlToContact(int $contact_id): Url {
    return Url::fromUri($this->url . 'app/contacts/' . $contact_id, ['attributes' => ['target' => 'blank']]);
  }

  /**
   * Create a URL to a campaign.
   *
   * @param int $campaign_id
   *   The id of the campaign to return.
   *
   * @return \Drupal\Core\Url
   *   Link to the overview of a campaign with given id on active campaign.
   */
  public function createUrlToCampaign(int $campaign_id): Url {
    return Url::fromUri($this->url . 'report/#/campaign/' . $campaign_id . '/overview', ['attributes' => ['target' => 'blank']]);
  }

}
