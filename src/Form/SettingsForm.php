<?php

namespace Drupal\activecampaign\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Active campaign settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ActiveCampaign url'),
      '#default_value' => $this->config('activecampaign.settings')->get('url'),
      '#description' => $this->t('Your active campaign url. e.g. https://XXX123.activehosted.com'),
    ];

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Api connection'),
    ];
    $form['api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ActiveCampaign api url'),
      '#default_value' => $this->config('activecampaign.settings')->get('api_url'),
      '#description' => $this->t('Your ActiveCampaign api url. e.g. https://XXX123.api-us1.com<br/>Can be found in activecampaign at <i>Settings > Developer</i>.'),
    ];

    $form['api']['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ActiveCampaign api key'),
      '#default_value' => $this->config('activecampaign.settings')->get('api_key'),
      '#description' => $this->t('Can be found in ActiveCampaign at <i>Settings > Developer</i>.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['activecampaign.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'activecampaign_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('activecampaign.settings')
      ->set('url', $form_state->getValue('url'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
