<?php

namespace Drupal\activecampaign_webform\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformYaml;
use Drupal\webform\webformSubmissionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Form submission handler.
 *
 * @WebformHandler(
 *   id = "activecampaign_contact",
 *   label = @Translation("Active Campaign"),
 *   category = @Translation("Automated marketing"),
 *   description = @Translation("Send submission data to Active Campaign"),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class ActiveCampaignFormHandler extends WebformHandlerBase {

  /**
   * The active campaign api service.
   *
   * @var \Drupal\activecampaign\ActiveCampaignApi
   */
  protected $activeCampaignApi;

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->activeCampaignApi = $container->get('activecampaign.api');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'email_field' => NULL,
      'first_name_field' => NULL,
      'last_name_field' => NULL,
      'debug' => FALSE,
      'custom_active_campaign_field' => "field[active_campaign_field_id,0]: '[webform_field_machine_name]'",
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $elements = $this->webform->getElementsInitializedAndFlattened();

    // Add the select none option to the list.
    $options = ['' => $this->t('None')];

    // Add every field to the list as a select option.
    foreach ($elements as $key => $element) {
      // Check if the field is an element field and not a layout field.
      if (isset($element['#title'])) {
        $options[$key] = $element['#title'];
      }
    }

    $form['email_field'] = [
      '#title' => $this->t('Email field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the email field to sync.'),
      '#default_value' => $this->configuration['email_field'],
      '#required' => TRUE,
    ];
    $form['first_name_field'] = [
      '#title' => $this->t('First name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the first name field to sync.'),
      '#default_value' => $this->configuration['first_name_field'],
    ];
    $form['last_name_field'] = [
      '#title' => $this->t('Last name field'),
      "#type" => "select",
      "#options" => $options,
      '#description' => $this->t('Choose the last name field to sync.'),
      '#default_value' => $this->configuration['last_name_field'],
    ];

    // Enable the debug option.
    $form['development'] = [
      '#type' => 'details',
      '#title' => $this->t('Development settings'),
    ];
    $form['development']['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debugging'),
      '#description' => $this->t('If checked, posted submissions will be displayed onscreen to all users.'),
      '#return_value' => TRUE,
      '#default_value' => $this->configuration['debug'],
    ];
    $form['development']['custom_active_campaign_field'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Active campaign submission data mapping'),
      '#description' => $this->t("To send additional fields other than the three basic fields provided, the custom yaml mapping can be used.
      The following syntax is required:
      <code>field[active_campaign_field_id,0]: '[webform_field_machine_name]'<code>.
      Examples <code>field[345,0]: '[field_machine_name]'<code> or <code>field[%PERS_1%,0]: '[field_machine_name]'<code>.
      More info can be found at the active campaign api 1, contact sync documentation."),
      '#default_value' => $this->configuration['custom_active_campaign_field'],
    ];

    return $this->setSettingsParents($form);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

    parent::submitConfigurationForm($form, $form_state);

    // Save the configuration settings.
    $this->applyFormStateToConfiguration($form_state);

    // Save debug settings.
    $this->configuration['debug'] = (bool) $this->configuration['debug'];

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission) {

    $configuration = $this->getConfiguration();
    $settings = $configuration['settings'];

    $data = $webform_submission->getData();

    // Retrieving the configured active campaign fields from the data.
    $email_key = $settings['email_field'];
    $first_name_key = $settings['first_name_field'];
    $last_name_key = $settings['last_name_field'];
    $custom_active_campaign_field = $settings['custom_active_campaign_field'];

    // Email field doesn't need to be checked, because it's required.
    $contact_properties['email'] = $data[$email_key];

    // Check if first name field has a value.
    if (isset($data[$first_name_key]) && !empty($data[$first_name_key])) {
      $contact_properties['first_name'] = $data[$first_name_key];
    }

    // Check if last name field has a value.
    if (isset($data[$last_name_key]) && !empty($data[$last_name_key])) {
      $contact_properties['last_name'] = $data[$last_name_key];
    }

    $custom_active_campaign_fields = Yaml::parse($custom_active_campaign_field);

    if (!empty($custom_active_campaign_fields)) {
      // Check if the default value was changed.
      if (!in_array('field[active_campaign_field_id,0]', $custom_active_campaign_fields)) {

        foreach ($custom_active_campaign_fields as $key => $value) {
          // Remove the unneeded characters from the webform id.
          $webform_id = str_replace('[', '', $value);
          $webform_id = str_replace(']', '', $webform_id);

          // Check if the mapped field has data to send.
          if (!empty($data[$webform_id])) {
            $contact_properties[$key] = $data[$webform_id];
          }
        }
      }
    }

    try {
      $response = $this->activeCampaignApi->syncContact($contact_properties);
      if (!$response->success) {
        $this->getLogger('activecampaign_webform')->error($response->error);
      }
    }
    catch (\Exception $exception) {
      $this->getLogger('activecampaign_webform')->error($exception->getMessage());
    }

    if ($settings['debug'] === TRUE) {
      $build = [
        'form_values_label' => ['#markup' => $this->t('Submitted form values are:')],
        'form_values_data' => [
          '#markup' => WebformYaml::encode($data),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
        'campaign_values_label' => ['#markup' => $this->t('Submitted active campaign values are:')],
        'campaign_values_data' => [
          '#markup' => WebformYaml::encode($contact_properties),
          '#prefix' => '<pre>',
          '#suffix' => '</pre>',
        ],
      ];

      $message = $this->renderer->renderPlain($build);
      $this->getLogger('activecampaign_webform')->debug(
        'Debugging enabled: </br>' . $message
      );
    }

  }

}
