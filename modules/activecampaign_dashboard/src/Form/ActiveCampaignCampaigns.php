<?php

namespace Drupal\activecampaign_dashboard\Form;

use Drupal\Core\Link;

/**
 * Adds a campaigns dashboard.
 */
class ActiveCampaignCampaigns extends ActiveCampaignDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['table_fields'] = [
      $this->t('Name'),
      $this->t('Date send'),
      $this->t('Emails send'),
      $this->t('Open rate'),
      $this->t('Click rate'),
      $this->t('Bounce rate'),
      $this->t('Unsubscribe rate'),
    ];

    // Check if we have an object containing our data instead of an error
    // message.
    $response = $this->api->getCampaigns([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = $response->total;
      foreach ($response->rows as $key => $campaign) {
        // Check if the row is actual data or metadata from the api.
        if (is_numeric($key)) {
          $data['rows'][$key] = [
            Link::fromTextAndUrl($campaign->name, $this->api->createUrlToCampaign($campaign->id)),
            $campaign->sdate,
            $campaign->send_amt,
            $this->convertToRate($campaign->uniqueopens, $campaign->send_amt),
            $this->convertToRate($campaign->linkclicks, $campaign->send_amt),
            $this->convertToRate($campaign->hardbounces, $campaign->send_amt),
            $this->convertToRate($campaign->unsubscribes, $campaign->send_amt),
          ];

        }
      }
    }

    return $data;
  }

  /**
   * Calculates the percentage rate of a field.
   *
   * @param int $amount
   *   The amount to use in the calculation.
   * @param int $total
   *   The total to use in the calculation.
   *
   * @return string
   *   Returns the field rate or defaults to "-", if the total given is zero.
   */
  private function convertToRate(int $amount, int $total): string {
    if ($total > 0) {
      $rate = ($amount / $total) * 100;
      return round($rate, 2) . '%';
    }
    else {
      return '-';
    }
  }

}
