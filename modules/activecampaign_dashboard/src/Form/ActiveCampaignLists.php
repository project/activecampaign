<?php

namespace Drupal\activecampaign_dashboard\Form;

use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Adds a lists' dashboard.
 */
class ActiveCampaignLists extends ActiveCampaignDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['table_fields'] = [
      $this->t('Name'),
      $this->t('Subscribers'),
      $this->t('Active Subscribers'),
    ];

    $response = $this->api->getLists([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = $response->total;
      foreach ($response->rows as $key => $list) {
        // Check if the row is actual data or metadata from the api.
        if (is_numeric($key)) {
          $url = Url::fromUri($list->url);
          $data['rows'][$key] = [
            Link::fromTextAndUrl($list->name, $url),
            $list->subscribers,
            $list->subscribers_active,
          ];

        }
      }
    }

    return $data;
  }

}
