<?php

namespace Drupal\activecampaign_dashboard\Form;

use Drupal\Core\Link;

/**
 * Adds a contacts dashboard.
 */
class ActiveCampaignContacts extends ActiveCampaignDashboard {

  /**
   * {@inheritDoc}
   */
  protected function getTableData(int $page = 0, int $limit = 20): array {
    $data['table_fields'] = [
      $this->t('Email'),
      $this->t('First Name'),
      $this->t('Last Name'),
      $this->t('Created'),
    ];

    // Check if we have an object containing our data instead of an error
    // message.
    $response = $this->api->getContacts([], $page, $limit);
    if (is_string($response)) {
      $this->messenger()->addError($response);
    }
    else {
      $data['total'] = $response->total;
      foreach ($response->rows as $key => $contact) {
        // Check if the row is actual data or metadata from the api.
        if (is_numeric($key)) {
          $data['rows'][$key] = [
            Link::fromTextAndUrl($contact->email, $this->api->createUrlToContact($contact->id)),
            $contact->first_name,
            $contact->last_name,
            $contact->cdate,
          ];

        }
      }
    }

    return $data;
  }

}
