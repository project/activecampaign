# Active campaign

Provides a field that shows an Active Campaign form,
a webform handler to send data to Active Campaign and three dashboard pages
displaying Active Campaign information.

## Usage

Configure the Activecampaign url, api url, and api key at
`/admin/config/services/activecampaign`

To embed forms into an entity just add an ActiveCampaign field to this entity.

Activate the Active Campaign widget and formatter.

To embed forms into layout builder, a custom block type must first be created,
after which an ActiveCampaign field can be added to this block.

The dashboard pages are located at these urls respectively

 * Dashboards dashboard page: `/admin/activecampaign/campaigns`
 * Contacts dashboard page: `/admin/activecampaign/campaigns`
 * Lists dashboard page: `/admin/activecampaign/lists`

To use the webformhandler, you need to specify the fields from which data should
be forwarded to ActiveCampaign, the default fields can be chosen via select
list.

When using the custom yaml mapping the following syntax is required:
 * `field[active_campaign_field_id,0]: '[webform_field_machine_name]'`.

Examples using ActiveCampaign custom mapping:
 * Field ID: `field[345,0]: '[webform_field_machine_name]'`
 * Personalization Tag: `field[%PERS_1%,0]: '[webform_field_machine_name]'`

More info can be found at the ActiveCampaign API pages,
[contact sync documentation](https://www.activecampaign.com/api/example.php?call=contact_sync)
and info on where to find the Personalization Tags can be found
[here](https://help.activecampaign.com/hc/en-us/articles/221433307-Custom-contact-field-overview#edit-or-delete-a-custom-contact-field-0-5).
